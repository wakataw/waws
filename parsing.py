from backend.whatsapp_binary_reader import whatsapp_read_binary
from glob import glob

for filename in glob("message/*"):
    with open(filename, 'rb') as f:
        try:
            content = whatsapp_read_binary(f.read())
            print(filename, content)
        except Exception as e:
            print(filename, e)

