import json

import pyqrcode
import websocket
import base64
import os
import time
import curve25519
import hashlib
import hmac

from pprint import pprint
from Crypto.Cipher import AES
from backend.whatsapp_binary_reader import whatsapp_read_binary

# Login Process

CLIENT_ID = base64.b64encode(os.urandom(16)).decode('utf8')
MESSAGE_TAG = int(time.time())
PRIVATE_KEY = curve25519.Private()
PUBLIC_KEY = PRIVATE_KEY.get_public()

print(
    "Client id", CLIENT_ID, '\n',
    "Message Tag", MESSAGE_TAG, '\n',
    "Private Key", base64.b64encode(PRIVATE_KEY.serialize()).decode('utf8'), '\n',
    "Public Key", base64.b64encode(PUBLIC_KEY.serialize()).decode('utf8'), '\n'
)

ws = websocket.WebSocket()
ws.connect(
    "wss://web.whatsapp.com/ws",
    origin="https://web.whatsapp.com"
)

login_message = '{MESSAGE_TAG},["admin","init",[0,3,3790],' \
               '["Wakataw Project Test Browser","Wakataw Project"],' \
                '"{CLIENT_ID}",true]'.format(MESSAGE_TAG=MESSAGE_TAG, CLIENT_ID=CLIENT_ID)

print(login_message)

ws.send(login_message)
_, result = ws.recv().split(',', 1)
result = json.loads(result)

print("Login Result")
pprint(result)

code = ','.join([result['ref'], base64.b64encode(PUBLIC_KEY.serialize()).decode('utf8'), CLIENT_ID])
qr = pyqrcode.create(code)
qr.png('qrcode.png', scale=6)
print("QR Code generated")

_, result = ws.recv().split(',', 1)
result = json.loads(result)
pprint(result)

# Key Generation

secret = base64.b64decode(result[1]['secret'])
print("Secret:", secret)
print("Secret length", len(secret))

shared_secret = PRIVATE_KEY.get_shared_key(curve25519.Public(secret[:32]), lambda x: x)


def hmac_sha256(key, sign):
    return hmac.new(key, sign, hashlib.sha256).digest()


def hkdf(key, length, app_info=b""):
    key = hmac_sha256(b"\0"*32, key)
    key_stream = b""
    key_block = b""
    block_index = 1
    while len(key_stream) < length:
        key_block = hmac.new(key, msg=key_block+app_info+chr(block_index).encode(), digestmod=hashlib.sha256).digest()
        block_index += 1
        key_stream += key_block
    return key_stream[:length]


shared_secret_extended = hkdf(shared_secret, 80)

validate_secret = hmac_sha256(shared_secret_extended[32:64], secret[:32]+secret[64:]) == secret[32:64]

print(validate_secret)


encrypted_key = shared_secret_extended[64:] + secret[64:]


def AESUnpad(s):
    return s[:-ord(s[len(s)-1:])]


def AESDecrypt(key, ciphertext):
    iv = ciphertext[:AES.block_size]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    plaintext = cipher.decrypt(ciphertext[AES.block_size:])
    return AESUnpad(plaintext)


decrypted_key = AESDecrypt(shared_secret_extended[:32], encrypted_key)

enc_key = decrypted_key[:32]
mac_key = decrypted_key[32:64]

print("encrypted key", enc_key)
print("mac key", mac_key)


def validate_message(content):
    return hmac_sha256(mac_key, content[32:]) == content[:32]


while True:
    try:
        result = ws.recv()
        if isinstance(result, bytes):
            _, result = result.split(b",", 1)
            is_valid = validate_message(result)
            if is_valid:
                content = AESDecrypt(enc_key, result[32:])

                with open("message/"+str(int(time.time()))+_.decode(), 'wb') as f:
                    f.write(content)

                try:
                    parsed_content = whatsapp_read_binary(content)
                except Exception as e:
                    parsed_content = str(e)

                print("Key:", _, "Content:", parsed_content)

    except KeyboardInterrupt:
        break
    except Exception as e:
        print(e)
        pass


print("Logout..")
ws.send('goodby,,["admin", "Conn", "disconnect"]')
result = ws.recv()
ws.close()
