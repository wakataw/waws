from whatsapp_defines import WATags, WASingleByteTokens, WADoubleByteTokens, WAWebMessageInfo


class WABinaryReader:
    def __init__(self, data):
        self.data = data
        self.index = 0

    def check_eos(self, length):
        if self.index + length > len(self.data):
            raise EOFError("end of stream reached")

    def read_byte(self):
        self.check_eos(1)
        ret = self.data[self.index]
        self.index += 1
        return ret

    def read_int_n(self, n, little_endian=False):
        self.check_eos(n)
        ret = 0
        for i in range(n):
            curr_shift = i if little_endian else n - 1 - i
            ret |= ord(self.data[self.index + i]) << (curr_shift * 8)
        self.index += n
        return ret

    def read_int16(self, little_endian=False):
        return self.read_int_n(2, little_endian)

    def read_int20(self):
        self.check_eos(3)
        ret = ((ord(self.data[self.index]) & 15) << 16) + (ord(self.data[self.index + 1]) << 8) + ord(
            self.data[self.index + 2])
        self.index += 3
        return ret

    def read_int32(self, little_endian=False):
        return self.read_int_n(4, little_endian)

    def read_int64(self, little_endian=False):
        return self.read_int_n(8, little_endian)

    def read_packed8(self, tag):
        start_byte = self.read_byte()
        ret = ""
        for i in range(start_byte & 127):
            curr_byte = self.read_byte()
            ret += self.unpack_byte(tag, (curr_byte & 0xF0) >> 4) + self.unpack_byte(tag, curr_byte & 0x0F)
        if (start_byte >> 7) == 0:
            ret = ret[:len(ret) - 1]
        return ret

    def unpack_byte(self, tag, value):
        if tag == WATags.NIBBLE_8:
            return self.unpack_nibble(value)
        elif tag == WATags.HEX_8:
            return self.unpack_hex(value)

    #
    # def read_ranged_var_int(self, min_val, max_val, desc="unknown"):
    #     ret = self.readVarInt()
    #     if ret < min_val or ret >= max_val:
    #         raise ValueError("varint for " + desc + " is out of bounds: " + str(ret))
    #     return ret

    def read_list_size(self, tag):
        if tag == WATags.LIST_EMPTY:
            return 0
        elif tag == WATags.LIST_8:
            return self.read_byte()
        elif tag == WATags.LIST_16:
            return self.read_int16()
        raise ValueError("invalid tag for list size: " + str(tag))

    def read_string(self, tag):
        if 3 <= tag <= 235:
            token = self.get_token(tag)
            if token == "s.whatsapp.net":
                token = "c.us"
            return token

        if tag == WATags.DICTIONARY_0 or tag == WATags.DICTIONARY_1 \
                or tag == WATags.DICTIONARY_2 or tag == WATags.DICTIONARY_3:
            return self.get_token_double(tag - WATags.DICTIONARY_0, self.read_byte())
        elif tag == WATags.LIST_EMPTY:
            return
        elif tag == WATags.BINARY_8:
            return self.read_string_from_chars(self.read_byte())
        elif tag == WATags.BINARY_20:
            return self.read_string_from_chars(self.read_int20())
        elif tag == WATags.BINARY_32:
            return self.read_string_from_chars(self.read_int32())
        elif tag == WATags.JID_PAIR:
            i = self.read_string(self.read_byte())
            j = self.read_string(self.read_byte())
            if i is None or j is None:
                raise ValueError("invalid jid pair: " + str(i) + ", " + str(j))
            return i + "@" + j
        elif tag == WATags.NIBBLE_8 or tag == WATags.HEX_8:
            return self.read_packed8(tag)
        else:
            raise ValueError("invalid string with tag " + str(tag))

    def read_string_from_chars(self, length):
        self.check_eos(length)
        ret = self.data[self.index:self.index + length]
        self.index += length
        return ret

    def read_attributes(self, n):
        ret = {}
        if n == 0:
            return
        for i in range(n):
            index = self.read_string(self.read_byte())
            ret[index] = self.read_string(self.read_byte())
        return ret

    def read_list(self, tag):
        ret = []
        for i in range(self.read_list_size(tag)):
            ret.append(self.read_node())
        return ret

    def read_node(self):
        list_size = self.read_list_size(self.read_byte())
        descr_tag = self.read_byte()

        if descr_tag == WATags.STREAM_END:
            raise ValueError("unexpected stream end")

        descr = self.read_string(descr_tag)

        if list_size == 0 or not descr:
            raise ValueError("invalid node")
        attrs = self.read_attributes((list_size - 1) >> 1)
        if list_size % 2 == 1:
            return [descr, attrs, None]

        tag = self.read_byte()
        if self.is_list_tag(tag):
            content = self.read_list(tag)
        elif tag == WATags.BINARY_8:
            content = self.read_bytes(self.read_byte())
        elif tag == WATags.BINARY_20:
            content = self.read_bytes(self.read_int20())
        elif tag == WATags.BINARY_32:
            content = self.read_bytes(self.read_int32())
        else:
            content = self.read_string(tag)
        return [descr, attrs, content]

    def read_bytes(self, n):
        ret = ""
        for i in range(n):
            ret += chr(self.read_byte())
        return ret

    @staticmethod
    def unpack_nibble(value):
        if value >= 0 and value <= 9:
            return chr(ord('0') + value)
        elif value == 10:
            return "-"
        elif value == 11:
            return "."
        elif value == 15:
            return "\0"
        raise ValueError("invalid nibble to unpack: " + value)

    @staticmethod
    def unpack_hex(value):
        if value < 0 or value > 15:
            raise ValueError("invalid hex to unpack: " + str(value))
        if value < 10:
            return chr(ord('0') + value)
        else:
            return chr(ord('A') + value - 10)

    @staticmethod
    def is_list_tag(tag):
        return tag == WATags.LIST_EMPTY or tag == WATags.LIST_8 or tag == WATags.LIST_16

    @staticmethod
    def get_token(index):
        if index < 3 or index >= len(WASingleByteTokens):
            raise ValueError("invalid token index: " + str(index))
        return WASingleByteTokens[index]

    @staticmethod
    def get_token_double(index1, index2):
        n = 256 * index1 + index2
        if n < 0 or n >= len(WADoubleByteTokens):
            raise ValueError("invalid token index: " + str(n))
        return WADoubleByteTokens[n]


def whatsapp_read_message_array(msgs):
    if not isinstance(msgs, list):
        return msgs
    ret = []
    for x in msgs:
        ret.append(WAWebMessageInfo.decode(x[2]) if isinstance(x, list) and x[0] == "message" else x)
    return ret


def whatsapp_read_binary(data, with_messages=False):
    node = WABinaryReader(data).read_node()
    if with_messages and node is not None and isinstance(node, list) and node[1] is not None:
        node[2] = whatsapp_read_message_array(node[2])
    return node
